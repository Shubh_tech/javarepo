class SubString{
    /*
    
    Substring : 
    * A part of String called Substring. In other word Substring is a subset of another string
    * indexing starts from : 0
    
    */
    public static void main(String ar[]){
        String s1 = "Shubham";
        String s2 = "ShubhamPanchal";

        System.out.println(s2.substring(0,7)); //Shubham
        System.out.println(s2.substring(7));   //Panchal
        System.out.println(s2.substring(0,7)+" "+s2.substring(7)); //Shubham panchal 
    }
}