class CompareMethod{
    /*
    There are three ways to compare string in java:
    * By equals() method
    * By = = operator
    * By compareTo() method
    ____________________________________________________________________________________
    String equals method
    * public boolean equals(Object another) compares this string to the specified object.
    * public boolean equalsIgnoreCase(String another) compares this String to another string,    ignoring case.

    ____________________________________________________________________________________
    String compare by == operator
    * The = = operator compares references not values.

    ____________________________________________________________________________________
    compareTo() method
    * The String compareTo() method compares values lexicographically and returns an integer value that describes if first string is less than, equal to or greater than second string.

    * Suppose s1 and s2 are two string variables. If:
      s1 == s2 :0
      s1 > s2   :positive value
      s1 < s2   :negative value

    */  
      void equal(){
           String s1 = "Shubham";
           String s2 = "SHUBHAM";
           System.out.println(s1.equalsIgnoreCase(s2)); //true
           String s3 = new String("Shubham");
           System.out.println(s1.equals(s3)); //true
      }
      void equal_equal(){
          String s1 = "Shubham";  
          String s2 = "Shubham";
          String s3 = new String("Shubham"); //If we compare s1 and s3 so false is return bcz s1 is in scp and s2 in heap area both are in diffrent memory
          if(s1==s2){               
              System.out.println("true");   //return true because both s1 and s2 refer same object. and the object created in scp.
          }
          else{
              System.out.println("false");
          }
          
      }
      void compareToMeth(){
          String s1 = "Shubham";
          String s2 = new String("Shubham");
          String s3 = "Panchal";
          

          System.out.println(s1.compareTo(s2)); //If both values are equal return 0
          System.out.println(s1.compareTo(s3)); //If both values are equal return 0
        //   System.out.println(s1.compareTo(s2)); //If both values are equal return 0
      }


      public static void main(String ar[]){
          CompareMethod c = new CompareMethod();
          //c.equal();
          //c.equal_equal();
          c.compareToMeth();
      }

}