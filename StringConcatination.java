class StringConcatination{
    /*
    String concatimation in java
    * In java, string concatenation forms a new string that is the combination of multiple strings. There are two ways to concat string in java:

    * By + (string concatenation) operator
    * By concat() method 
    
    */
    public static void main(String arg[]){
        String s1 = "Shubham";
        String s2 = "Panchal";
        System.out.println(s1+s2);
        System.out.println("Rahul"+"panchal");
        System.out.println(s1.concat(s2));
    }
}